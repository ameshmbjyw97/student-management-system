﻿using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace StudentRegSys
{
    public partial class Student : MaterialForm
    {
        private int Id;
        private SqlConnection con = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=" + System.IO.Path.GetDirectoryName(Application.ExecutablePath) + "\\SUDB.mdf; Integrated Security = True; Connect Timeout = 30");
        private SqlCommand cmd;

        public Student(int user)
        {
            InitializeComponent();
            Id = user;
        }
        private void Student_Load(object sender, System.EventArgs e)
        {
            btnPassword.AutoSize = false;
            btnPassword.Width = 150;
            btnPay.AutoSize = false;
            btnPay.Width = 150;

            cmd = new SqlCommand("select Year from Student where Reg_No=" + Id, con);
            con.Open();
            int year = (int)cmd.ExecuteScalar();
            con.Close();

            try
            {
                cmd = new SqlCommand("select * from Student where Reg_No="+Id, con);
                con.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    lblReg.Text = reader["Reg_No"].ToString();
                    lblName.Text = reader["FName"].ToString()+" "+ reader["LName"].ToString();
                    lblGender.Text = (reader["Gender"].ToString().ToLower()=="true") ? "Male" : "Female";
                    lblDOB.Text = reader["DOB"].ToString();
                    lblAddress.Text = reader["Address"].ToString();
                    lblEmail.Text = reader["Email"].ToString();
                    lblPhone.Text = reader["Phone"].ToString();
                    lblYear.Text = reader["Year"].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Retrieving data from Database", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            con.Close();
            btnReg.AutoSize = false;
            btnReg.Width = 150;
            SqlDataAdapter adt = new SqlDataAdapter("select * from Module", con);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                adt.Fill(dt);
                con.Close();
                datagrid.DataSource = dt;
                for (int i = 0; i < 6; i++)
                    datagrid.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Retrieving data from Database", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            con.Close();

            DateTime start = new DateTime();
            DateTime end = new DateTime();

            try
            {
                cmd = new SqlCommand("select StartDate,EndDate from General where Id=1", con);
                con.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    start = DateTime.ParseExact(reader["StartDate"].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    end = DateTime.ParseExact(reader["EndDate"].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Retrieving data from Database", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            con.Close();

            List<string> modulelist = new List<string>();
            string[] selectedModulelist = new string[4];
            try
            {
                cmd = new SqlCommand("select MId from Module where Year=" + year, con);
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                    modulelist.Add(dr.GetString(0));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Retrieving data from Database", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            con.Close();

            Module1.Items.AddRange(modulelist.ToArray());
            Module2.Items.AddRange(modulelist.ToArray());
            Module3.Items.AddRange(modulelist.ToArray());
            Module4.Items.AddRange(modulelist.ToArray());
            Module5.Items.AddRange(modulelist.ToArray());
            Module6.Items.AddRange(modulelist.ToArray());

            try
            {
                cmd = new SqlCommand("select Module1,Module2,Module3,Module4,Module5,Module6 from Student where Reg_No=" + Id, con);
                con.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    selectedModulelist[0] = reader["Module1"].ToString();
                    selectedModulelist[1] = reader["Module2"].ToString();
                    selectedModulelist[2] = reader["Module3"].ToString();
                    selectedModulelist[3] = reader["Module4"].ToString();
                    Module1.SelectedItem = selectedModulelist[0];
                    Module2.SelectedItem = selectedModulelist[1];
                    Module3.SelectedItem = selectedModulelist[2];
                    Module4.SelectedItem = selectedModulelist[3];
                    Module5.SelectedItem = reader["Module5"].ToString();
                    Module6.SelectedItem = reader["Module6"].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Retrieving data from Database", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            con.Close();

            if (start > DateTime.Today)
            {
                panel2.Enabled = false;
                reg.Text = "Registration will be open on " + start.ToString("dd-MM-yyyy");
            }
            else if (end < DateTime.Today)
            {
                panel2.Enabled = false;
                reg.Text = "Registration is closed.";
                reg.Location=new Point(reg.Location.X +100, reg.Location.Y);
                lblBillDesc.Visible = false;
                lblBill.Visible = true;
                btnPay.Visible = true;
                float total = calcBill(selectedModulelist);
                lblBill.Text += " " + total.ToString();
                SqlCommand cmd = new SqlCommand("update Billing set Bill=" + total + " where Id=" + Id, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            else
            {
                panel2.Enabled = true;
                reg.Text = "Registration will be closed on "+end.ToString("dd-MM-yyyy");
            }
        }
        private void lblYear_Click(object sender, System.EventArgs e)
        {
            MessageBox.Show("Please contact the Registrar’s office to change your details.", "Details", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void btnPassword_Click(object sender, EventArgs e)
        {
            string pass = "";
            try
            {
                cmd = new SqlCommand("select Password from Auth where Id=" + Id, con);
                con.Open();
                pass = cmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Changing Password", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            con.Close();
            if (txtCurrent.Text != pass)
                MessageBox.Show("The current password is incorrect. Contact the Registrar’s office if you have forgotten your password.", "Changing Password", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else if (txtConfirm.Text != txtNew.Text)
                MessageBox.Show("Passwords does not match", "Changing Password", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else if (txtNew.TextLength < 6)
                MessageBox.Show("Passwords should be longer than 6 characters", "Changing Password", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                SqlCommand cmd = new SqlCommand("update Auth set Password='" + txtNew.Text + "' where Id=" + Id, con);
                try
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Password Changed", "Changing Password", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Changing Password", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                con.Close();
            }
        }
        private void btnReg_Click(object sender, EventArgs e)
        {
            if (Module1.SelectedItem == Module2.SelectedItem || Module1.SelectedItem == Module3.SelectedItem || Module1.SelectedItem == Module4.SelectedItem || Module1.SelectedItem == Module5.SelectedItem || Module1.SelectedItem == Module6.SelectedItem)
            {
                MessageBox.Show("You cannot select same module twice.", "Registraion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Module1.DroppedDown = true;
            }
            else if (Module2.SelectedItem == Module3.SelectedItem || Module2.SelectedItem == Module4.SelectedItem || Module2.SelectedItem == Module5.SelectedItem || Module2.SelectedItem == Module6.SelectedItem)
            {
                MessageBox.Show("You cannot select same module twice.", "Registraion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Module2.DroppedDown = true;
            }
            else if (Module3.SelectedItem == Module4.SelectedItem || Module3.SelectedItem == Module5.SelectedItem || Module3.SelectedItem == Module6.SelectedItem)
            {
                MessageBox.Show("You cannot select same module twice.", "Registraion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Module3.DroppedDown = true;
            }
            else if (Module4.SelectedItem == Module5.SelectedItem || Module4.SelectedItem == Module6.SelectedItem)
            {
                MessageBox.Show("You cannot select same module twice.", "Registraion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Module4.DroppedDown = true;
            }
            else if (Module5.SelectedItem == Module6.SelectedItem)
            {
                MessageBox.Show("You cannot select same module twice.", "Registraion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Module5.DroppedDown = true;
            }
            else
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("update Student set Module1='" + Module1.Text + "',Module2='" + Module2.Text + "',Module3='" + Module3.Text + "',Module4='" + Module4.Text + "',Module5='" + Module5.Text + "',Module6='" + Module6.Text + "' where Reg_No=" + Id, con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    MessageBox.Show("You have successfully registered.", "Students", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Registration", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            
        }
        private float calcBill(string[] list)
        {
            float result = 0;
            con.Open();
            cmd = new SqlCommand("select Cost from Module where MId='" + list[0]+ "'", con);
            result += float.Parse(cmd.ExecuteScalar().ToString());
            cmd = new SqlCommand("select Cost from Module where MId='" + list[1] + "'", con);
            result += float.Parse(cmd.ExecuteScalar().ToString());
            cmd = new SqlCommand("select Cost from Module where MId='" + list[2] + "'", con);
            result += float.Parse(cmd.ExecuteScalar().ToString());
            cmd = new SqlCommand("select Cost from Module where MId='" + list[3] + "'", con);
            result += float.Parse(cmd.ExecuteScalar().ToString());
            con.Close();
            return result;
        }

        private void LblregValue_Click(object sender, EventArgs e)
        {

        }
    }
}
