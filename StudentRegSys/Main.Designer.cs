﻿namespace StudentRegSys
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.materialTabControl1 = new MaterialSkin.Controls.MaterialTabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txtConfirm = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel35 = new MaterialSkin.Controls.MaterialLabel();
            this.txtUsername = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel24 = new MaterialSkin.Controls.MaterialLabel();
            this.btnUsername = new MaterialSkin.Controls.MaterialRaisedButton();
            this.txtCurrent = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtNew = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel23 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel25 = new MaterialSkin.Controls.MaterialLabel();
            this.btnPassword = new MaterialSkin.Controls.MaterialRaisedButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dtpEnds = new System.Windows.Forms.DateTimePicker();
            this.dtpBegins = new System.Windows.Forms.DateTimePicker();
            this.materialLabel32 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel33 = new MaterialSkin.Controls.MaterialLabel();
            this.btnChange = new MaterialSkin.Controls.MaterialRaisedButton();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtMYears = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtMCredits = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel22 = new MaterialSkin.Controls.MaterialLabel();
            this.pbxMSearch = new System.Windows.Forms.PictureBox();
            this.materialLabel26 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel27 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel28 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel30 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel31 = new MaterialSkin.Controls.MaterialLabel();
            this.btnMSave = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnMDelete = new MaterialSkin.Controls.MaterialRaisedButton();
            this.txtMCost = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtMHours = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtMName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtMId = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SModule6 = new System.Windows.Forms.TextBox();
            this.SModule5 = new System.Windows.Forms.TextBox();
            this.SModule4 = new System.Windows.Forms.TextBox();
            this.SModule3 = new System.Windows.Forms.TextBox();
            this.SModule2 = new System.Windows.Forms.TextBox();
            this.SModule1 = new System.Windows.Forms.TextBox();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.pbxSSearch = new System.Windows.Forms.PictureBox();
            this.materialLabel10 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel9 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel8 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel7 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel6 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.btnSSave = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnSDelete = new MaterialSkin.Controls.MaterialRaisedButton();
            this.cmbxSYear = new System.Windows.Forms.ComboBox();
            this.txtSPhone = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtSEmail = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtSAddress = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.dtpSDob = new System.Windows.Forms.DateTimePicker();
            this.radSF = new MaterialSkin.Controls.MaterialRadioButton();
            this.radSM = new MaterialSkin.Controls.MaterialRadioButton();
            this.txtSLname = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtSFname = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtSReg = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnPModules = new MaterialSkin.Controls.MaterialFlatButton();
            this.materialLabel21 = new MaterialSkin.Controls.MaterialLabel();
            this.txtPUsername = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtPQf = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel11 = new MaterialSkin.Controls.MaterialLabel();
            this.pbxPSearch = new System.Windows.Forms.PictureBox();
            this.materialLabel12 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel13 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel14 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel15 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel16 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel17 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel18 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel19 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel20 = new MaterialSkin.Controls.MaterialLabel();
            this.btnPSave = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnPDelete = new MaterialSkin.Controls.MaterialRaisedButton();
            this.txtPPhone = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtPEmail = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtPAddress = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.dtpPDob = new System.Windows.Forms.DateTimePicker();
            this.radPF = new MaterialSkin.Controls.MaterialRadioButton();
            this.radPM = new MaterialSkin.Controls.MaterialRadioButton();
            this.txtPLname = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtPFname = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtPReg = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnReset = new System.Windows.Forms.Button();
            this.txtReset = new System.Windows.Forms.TextBox();
            this.materialLabel36 = new MaterialSkin.Controls.MaterialLabel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnSearch = new MaterialSkin.Controls.MaterialRaisedButton();
            this.datagrid = new System.Windows.Forms.DataGridView();
            this.txtValue = new System.Windows.Forms.TextBox();
            this.cmbxSearchBy = new System.Windows.Forms.ComboBox();
            this.cmbxSearchTable = new System.Windows.Forms.ComboBox();
            this.materialLabel34 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel29 = new MaterialSkin.Controls.MaterialLabel();
            this.materialTabSelector1 = new MaterialSkin.Controls.MaterialTabSelector();
            this.materialTabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMSearch)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSSearch)).BeginInit();
            this.tabPage6.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxPSearch)).BeginInit();
            this.tabPage7.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datagrid)).BeginInit();
            this.SuspendLayout();
            // 
            // materialTabControl1
            // 
            this.materialTabControl1.Controls.Add(this.tabPage3);
            this.materialTabControl1.Controls.Add(this.tabPage4);
            this.materialTabControl1.Controls.Add(this.tabPage5);
            this.materialTabControl1.Controls.Add(this.tabPage6);
            this.materialTabControl1.Controls.Add(this.tabPage7);
            this.materialTabControl1.Depth = 0;
            this.materialTabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.materialTabControl1.Location = new System.Drawing.Point(0, 100);
            this.materialTabControl1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabControl1.Name = "materialTabControl1";
            this.materialTabControl1.SelectedIndex = 0;
            this.materialTabControl1.Size = new System.Drawing.Size(900, 580);
            this.materialTabControl1.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel5);
            this.tabPage3.Controls.Add(this.panel4);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(892, 554);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "General";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.txtConfirm);
            this.panel5.Controls.Add(this.materialLabel35);
            this.panel5.Controls.Add(this.txtUsername);
            this.panel5.Controls.Add(this.materialLabel24);
            this.panel5.Controls.Add(this.btnUsername);
            this.panel5.Controls.Add(this.txtCurrent);
            this.panel5.Controls.Add(this.txtNew);
            this.panel5.Controls.Add(this.materialLabel23);
            this.panel5.Controls.Add(this.materialLabel25);
            this.panel5.Controls.Add(this.btnPassword);
            this.panel5.Location = new System.Drawing.Point(46, 237);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(800, 326);
            this.panel5.TabIndex = 3;
            // 
            // txtConfirm
            // 
            this.txtConfirm.Depth = 0;
            this.txtConfirm.Hint = "";
            this.txtConfirm.Location = new System.Drawing.Point(212, 139);
            this.txtConfirm.MaxLength = 32767;
            this.txtConfirm.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtConfirm.Name = "txtConfirm";
            this.txtConfirm.PasswordChar = '\0';
            this.txtConfirm.SelectedText = "";
            this.txtConfirm.SelectionLength = 0;
            this.txtConfirm.SelectionStart = 0;
            this.txtConfirm.Size = new System.Drawing.Size(200, 23);
            this.txtConfirm.TabIndex = 56;
            this.txtConfirm.TabStop = false;
            this.txtConfirm.UseSystemPasswordChar = true;
            // 
            // materialLabel35
            // 
            this.materialLabel35.AutoSize = true;
            this.materialLabel35.Depth = 0;
            this.materialLabel35.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel35.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel35.Location = new System.Drawing.Point(20, 141);
            this.materialLabel35.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel35.Name = "materialLabel35";
            this.materialLabel35.Size = new System.Drawing.Size(137, 19);
            this.materialLabel35.TabIndex = 55;
            this.materialLabel35.Text = "Confirm Password:";
            // 
            // txtUsername
            // 
            this.txtUsername.Depth = 0;
            this.txtUsername.Hint = "";
            this.txtUsername.Location = new System.Drawing.Point(212, 239);
            this.txtUsername.MaxLength = 32767;
            this.txtUsername.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.PasswordChar = '\0';
            this.txtUsername.SelectedText = "";
            this.txtUsername.SelectionLength = 0;
            this.txtUsername.SelectionStart = 0;
            this.txtUsername.Size = new System.Drawing.Size(200, 23);
            this.txtUsername.TabIndex = 54;
            this.txtUsername.TabStop = false;
            this.txtUsername.UseSystemPasswordChar = false;
            // 
            // materialLabel24
            // 
            this.materialLabel24.AutoSize = true;
            this.materialLabel24.Depth = 0;
            this.materialLabel24.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel24.Location = new System.Drawing.Point(20, 241);
            this.materialLabel24.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel24.Name = "materialLabel24";
            this.materialLabel24.Size = new System.Drawing.Size(128, 19);
            this.materialLabel24.TabIndex = 53;
            this.materialLabel24.Text = "Admin Username:";
            // 
            // btnUsername
            // 
            this.btnUsername.AutoSize = true;
            this.btnUsername.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnUsername.Depth = 0;
            this.btnUsername.Icon = null;
            this.btnUsername.Location = new System.Drawing.Point(628, 259);
            this.btnUsername.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnUsername.Name = "btnUsername";
            this.btnUsername.Primary = true;
            this.btnUsername.Size = new System.Drawing.Size(151, 36);
            this.btnUsername.TabIndex = 52;
            this.btnUsername.Text = "Change Username";
            this.btnUsername.UseVisualStyleBackColor = true;
            this.btnUsername.Click += new System.EventHandler(this.btnUsername_Click);
            // 
            // txtCurrent
            // 
            this.txtCurrent.Depth = 0;
            this.txtCurrent.Hint = "";
            this.txtCurrent.Location = new System.Drawing.Point(212, 37);
            this.txtCurrent.MaxLength = 32767;
            this.txtCurrent.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtCurrent.Name = "txtCurrent";
            this.txtCurrent.PasswordChar = '\0';
            this.txtCurrent.SelectedText = "";
            this.txtCurrent.SelectionLength = 0;
            this.txtCurrent.SelectionStart = 0;
            this.txtCurrent.Size = new System.Drawing.Size(200, 23);
            this.txtCurrent.TabIndex = 51;
            this.txtCurrent.TabStop = false;
            this.txtCurrent.UseSystemPasswordChar = true;
            // 
            // txtNew
            // 
            this.txtNew.Depth = 0;
            this.txtNew.Hint = "";
            this.txtNew.Location = new System.Drawing.Point(212, 88);
            this.txtNew.MaxLength = 32767;
            this.txtNew.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtNew.Name = "txtNew";
            this.txtNew.PasswordChar = '\0';
            this.txtNew.SelectedText = "";
            this.txtNew.SelectionLength = 0;
            this.txtNew.SelectionStart = 0;
            this.txtNew.Size = new System.Drawing.Size(200, 23);
            this.txtNew.TabIndex = 50;
            this.txtNew.TabStop = false;
            this.txtNew.UseSystemPasswordChar = true;
            // 
            // materialLabel23
            // 
            this.materialLabel23.AutoSize = true;
            this.materialLabel23.Depth = 0;
            this.materialLabel23.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel23.Location = new System.Drawing.Point(20, 39);
            this.materialLabel23.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel23.Name = "materialLabel23";
            this.materialLabel23.Size = new System.Drawing.Size(132, 19);
            this.materialLabel23.TabIndex = 49;
            this.materialLabel23.Text = "Current Password:";
            // 
            // materialLabel25
            // 
            this.materialLabel25.AutoSize = true;
            this.materialLabel25.Depth = 0;
            this.materialLabel25.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel25.Location = new System.Drawing.Point(20, 90);
            this.materialLabel25.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel25.Name = "materialLabel25";
            this.materialLabel25.Size = new System.Drawing.Size(113, 19);
            this.materialLabel25.TabIndex = 47;
            this.materialLabel25.Text = "New Password:";
            // 
            // btnPassword
            // 
            this.btnPassword.AutoSize = true;
            this.btnPassword.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnPassword.Depth = 0;
            this.btnPassword.Icon = null;
            this.btnPassword.Location = new System.Drawing.Point(626, 162);
            this.btnPassword.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnPassword.Name = "btnPassword";
            this.btnPassword.Primary = true;
            this.btnPassword.Size = new System.Drawing.Size(153, 36);
            this.btnPassword.TabIndex = 46;
            this.btnPassword.Text = "Change Password";
            this.btnPassword.UseVisualStyleBackColor = true;
            this.btnPassword.Click += new System.EventHandler(this.btnPassword_Click);
            // 
            // panel4
            // 
            this.panel4.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.dtpEnds);
            this.panel4.Controls.Add(this.dtpBegins);
            this.panel4.Controls.Add(this.materialLabel32);
            this.panel4.Controls.Add(this.materialLabel33);
            this.panel4.Controls.Add(this.btnChange);
            this.panel4.Location = new System.Drawing.Point(46, 18);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(800, 198);
            this.panel4.TabIndex = 2;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel4_Paint);
            // 
            // dtpEnds
            // 
            this.dtpEnds.Location = new System.Drawing.Point(211, 93);
            this.dtpEnds.Name = "dtpEnds";
            this.dtpEnds.Size = new System.Drawing.Size(200, 20);
            this.dtpEnds.TabIndex = 21;
            // 
            // dtpBegins
            // 
            this.dtpBegins.Location = new System.Drawing.Point(211, 45);
            this.dtpBegins.Name = "dtpBegins";
            this.dtpBegins.Size = new System.Drawing.Size(200, 20);
            this.dtpBegins.TabIndex = 20;
            // 
            // materialLabel32
            // 
            this.materialLabel32.AutoSize = true;
            this.materialLabel32.Depth = 0;
            this.materialLabel32.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel32.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel32.Location = new System.Drawing.Point(20, 94);
            this.materialLabel32.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel32.Name = "materialLabel32";
            this.materialLabel32.Size = new System.Drawing.Size(131, 19);
            this.materialLabel32.TabIndex = 19;
            this.materialLabel32.Text = "Registration Ends:";
            // 
            // materialLabel33
            // 
            this.materialLabel33.AutoSize = true;
            this.materialLabel33.Depth = 0;
            this.materialLabel33.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel33.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel33.Location = new System.Drawing.Point(20, 46);
            this.materialLabel33.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel33.Name = "materialLabel33";
            this.materialLabel33.Size = new System.Drawing.Size(138, 19);
            this.materialLabel33.TabIndex = 18;
            this.materialLabel33.Text = "Registraion Begins:";
            // 
            // btnChange
            // 
            this.btnChange.AutoSize = true;
            this.btnChange.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnChange.Depth = 0;
            this.btnChange.Icon = null;
            this.btnChange.Location = new System.Drawing.Point(628, 135);
            this.btnChange.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnChange.Name = "btnChange";
            this.btnChange.Primary = true;
            this.btnChange.Size = new System.Drawing.Size(122, 36);
            this.btnChange.TabIndex = 16;
            this.btnChange.Text = "Change Dates";
            this.btnChange.UseVisualStyleBackColor = true;
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.panel3);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(892, 548);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Modules";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.txtMYears);
            this.panel3.Controls.Add(this.txtMCredits);
            this.panel3.Controls.Add(this.materialLabel22);
            this.panel3.Controls.Add(this.pbxMSearch);
            this.panel3.Controls.Add(this.materialLabel26);
            this.panel3.Controls.Add(this.materialLabel27);
            this.panel3.Controls.Add(this.materialLabel28);
            this.panel3.Controls.Add(this.materialLabel30);
            this.panel3.Controls.Add(this.materialLabel31);
            this.panel3.Controls.Add(this.btnMSave);
            this.panel3.Controls.Add(this.btnMDelete);
            this.panel3.Controls.Add(this.txtMCost);
            this.panel3.Controls.Add(this.txtMHours);
            this.panel3.Controls.Add(this.txtMName);
            this.panel3.Controls.Add(this.txtMId);
            this.panel3.Location = new System.Drawing.Point(46, 18);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(800, 393);
            this.panel3.TabIndex = 1;
            // 
            // txtMYears
            // 
            this.txtMYears.Depth = 0;
            this.txtMYears.Hint = "";
            this.txtMYears.Location = new System.Drawing.Point(144, 144);
            this.txtMYears.MaxLength = 32767;
            this.txtMYears.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtMYears.Name = "txtMYears";
            this.txtMYears.PasswordChar = '\0';
            this.txtMYears.SelectedText = "";
            this.txtMYears.SelectionLength = 0;
            this.txtMYears.SelectionStart = 0;
            this.txtMYears.Size = new System.Drawing.Size(200, 23);
            this.txtMYears.TabIndex = 44;
            this.txtMYears.TabStop = false;
            this.txtMYears.UseSystemPasswordChar = false;
            // 
            // txtMCredits
            // 
            this.txtMCredits.Depth = 0;
            this.txtMCredits.Hint = "";
            this.txtMCredits.Location = new System.Drawing.Point(144, 194);
            this.txtMCredits.MaxLength = 32767;
            this.txtMCredits.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtMCredits.Name = "txtMCredits";
            this.txtMCredits.PasswordChar = '\0';
            this.txtMCredits.SelectedText = "";
            this.txtMCredits.SelectionLength = 0;
            this.txtMCredits.SelectionStart = 0;
            this.txtMCredits.Size = new System.Drawing.Size(200, 23);
            this.txtMCredits.TabIndex = 43;
            this.txtMCredits.TabStop = false;
            this.txtMCredits.UseSystemPasswordChar = false;
            // 
            // materialLabel22
            // 
            this.materialLabel22.AutoSize = true;
            this.materialLabel22.Depth = 0;
            this.materialLabel22.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel22.Location = new System.Drawing.Point(20, 146);
            this.materialLabel22.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel22.Name = "materialLabel22";
            this.materialLabel22.Size = new System.Drawing.Size(43, 19);
            this.materialLabel22.TabIndex = 42;
            this.materialLabel22.Text = "Year:";
            // 
            // pbxMSearch
            // 
            this.pbxMSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbxMSearch.BackgroundImage")));
            this.pbxMSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxMSearch.Location = new System.Drawing.Point(359, 43);
            this.pbxMSearch.Name = "pbxMSearch";
            this.pbxMSearch.Size = new System.Drawing.Size(24, 24);
            this.pbxMSearch.TabIndex = 41;
            this.pbxMSearch.TabStop = false;
            this.pbxMSearch.Click += new System.EventHandler(this.pbxMSearch_Click);
            // 
            // materialLabel26
            // 
            this.materialLabel26.AutoSize = true;
            this.materialLabel26.Depth = 0;
            this.materialLabel26.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel26.Location = new System.Drawing.Point(20, 290);
            this.materialLabel26.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel26.Name = "materialLabel26";
            this.materialLabel26.Size = new System.Drawing.Size(45, 19);
            this.materialLabel26.TabIndex = 24;
            this.materialLabel26.Text = "Cost:";
            // 
            // materialLabel27
            // 
            this.materialLabel27.AutoSize = true;
            this.materialLabel27.Depth = 0;
            this.materialLabel27.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel27.Location = new System.Drawing.Point(20, 242);
            this.materialLabel27.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel27.Name = "materialLabel27";
            this.materialLabel27.Size = new System.Drawing.Size(54, 19);
            this.materialLabel27.TabIndex = 23;
            this.materialLabel27.Text = "Hours:";
            // 
            // materialLabel28
            // 
            this.materialLabel28.AutoSize = true;
            this.materialLabel28.Depth = 0;
            this.materialLabel28.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel28.Location = new System.Drawing.Point(20, 196);
            this.materialLabel28.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel28.Name = "materialLabel28";
            this.materialLabel28.Size = new System.Drawing.Size(61, 19);
            this.materialLabel28.TabIndex = 22;
            this.materialLabel28.Text = "Credits:";
            // 
            // materialLabel30
            // 
            this.materialLabel30.AutoSize = true;
            this.materialLabel30.Depth = 0;
            this.materialLabel30.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel30.Location = new System.Drawing.Point(20, 94);
            this.materialLabel30.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel30.Name = "materialLabel30";
            this.materialLabel30.Size = new System.Drawing.Size(53, 19);
            this.materialLabel30.TabIndex = 19;
            this.materialLabel30.Text = "Name:";
            // 
            // materialLabel31
            // 
            this.materialLabel31.AutoSize = true;
            this.materialLabel31.Depth = 0;
            this.materialLabel31.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel31.Location = new System.Drawing.Point(20, 46);
            this.materialLabel31.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel31.Name = "materialLabel31";
            this.materialLabel31.Size = new System.Drawing.Size(79, 19);
            this.materialLabel31.TabIndex = 18;
            this.materialLabel31.Text = "Module Id:";
            // 
            // btnMSave
            // 
            this.btnMSave.AutoSize = true;
            this.btnMSave.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnMSave.Depth = 0;
            this.btnMSave.Icon = null;
            this.btnMSave.Location = new System.Drawing.Point(473, 332);
            this.btnMSave.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnMSave.Name = "btnMSave";
            this.btnMSave.Primary = true;
            this.btnMSave.Size = new System.Drawing.Size(140, 36);
            this.btnMSave.TabIndex = 17;
            this.btnMSave.Text = "Save the Record";
            this.btnMSave.UseVisualStyleBackColor = true;
            this.btnMSave.Click += new System.EventHandler(this.btnMSave_Click);
            // 
            // btnMDelete
            // 
            this.btnMDelete.AutoSize = true;
            this.btnMDelete.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnMDelete.Depth = 0;
            this.btnMDelete.Icon = null;
            this.btnMDelete.Location = new System.Drawing.Point(628, 332);
            this.btnMDelete.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnMDelete.Name = "btnMDelete";
            this.btnMDelete.Primary = true;
            this.btnMDelete.Size = new System.Drawing.Size(154, 36);
            this.btnMDelete.TabIndex = 16;
            this.btnMDelete.Text = "Delete the Record";
            this.btnMDelete.UseVisualStyleBackColor = true;
            this.btnMDelete.Click += new System.EventHandler(this.btnMDelete_Click);
            // 
            // txtMCost
            // 
            this.txtMCost.Depth = 0;
            this.txtMCost.Hint = "";
            this.txtMCost.Location = new System.Drawing.Point(144, 288);
            this.txtMCost.MaxLength = 32767;
            this.txtMCost.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtMCost.Name = "txtMCost";
            this.txtMCost.PasswordChar = '\0';
            this.txtMCost.SelectedText = "";
            this.txtMCost.SelectionLength = 0;
            this.txtMCost.SelectionStart = 0;
            this.txtMCost.Size = new System.Drawing.Size(200, 23);
            this.txtMCost.TabIndex = 7;
            this.txtMCost.TabStop = false;
            this.txtMCost.UseSystemPasswordChar = false;
            // 
            // txtMHours
            // 
            this.txtMHours.Depth = 0;
            this.txtMHours.Hint = "";
            this.txtMHours.Location = new System.Drawing.Point(144, 240);
            this.txtMHours.MaxLength = 32767;
            this.txtMHours.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtMHours.Name = "txtMHours";
            this.txtMHours.PasswordChar = '\0';
            this.txtMHours.SelectedText = "";
            this.txtMHours.SelectionLength = 0;
            this.txtMHours.SelectionStart = 0;
            this.txtMHours.Size = new System.Drawing.Size(200, 23);
            this.txtMHours.TabIndex = 6;
            this.txtMHours.TabStop = false;
            this.txtMHours.UseSystemPasswordChar = false;
            // 
            // txtMName
            // 
            this.txtMName.Depth = 0;
            this.txtMName.Hint = "";
            this.txtMName.Location = new System.Drawing.Point(144, 92);
            this.txtMName.MaxLength = 32767;
            this.txtMName.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtMName.Name = "txtMName";
            this.txtMName.PasswordChar = '\0';
            this.txtMName.SelectedText = "";
            this.txtMName.SelectionLength = 0;
            this.txtMName.SelectionStart = 0;
            this.txtMName.Size = new System.Drawing.Size(200, 23);
            this.txtMName.TabIndex = 1;
            this.txtMName.TabStop = false;
            this.txtMName.UseSystemPasswordChar = false;
            // 
            // txtMId
            // 
            this.txtMId.Depth = 0;
            this.txtMId.Hint = "";
            this.txtMId.Location = new System.Drawing.Point(144, 44);
            this.txtMId.MaxLength = 32767;
            this.txtMId.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtMId.Name = "txtMId";
            this.txtMId.PasswordChar = '\0';
            this.txtMId.SelectedText = "";
            this.txtMId.SelectionLength = 0;
            this.txtMId.SelectionStart = 0;
            this.txtMId.Size = new System.Drawing.Size(200, 23);
            this.txtMId.TabIndex = 0;
            this.txtMId.TabStop = false;
            this.txtMId.UseSystemPasswordChar = false;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.panel1);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(892, 548);
            this.tabPage5.TabIndex = 2;
            this.tabPage5.Text = "Students";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.SModule6);
            this.panel1.Controls.Add(this.SModule5);
            this.panel1.Controls.Add(this.SModule4);
            this.panel1.Controls.Add(this.SModule3);
            this.panel1.Controls.Add(this.SModule2);
            this.panel1.Controls.Add(this.SModule1);
            this.panel1.Controls.Add(this.materialLabel4);
            this.panel1.Controls.Add(this.pbxSSearch);
            this.panel1.Controls.Add(this.materialLabel10);
            this.panel1.Controls.Add(this.materialLabel9);
            this.panel1.Controls.Add(this.materialLabel8);
            this.panel1.Controls.Add(this.materialLabel7);
            this.panel1.Controls.Add(this.materialLabel6);
            this.panel1.Controls.Add(this.materialLabel5);
            this.panel1.Controls.Add(this.materialLabel3);
            this.panel1.Controls.Add(this.materialLabel2);
            this.panel1.Controls.Add(this.materialLabel1);
            this.panel1.Controls.Add(this.btnSSave);
            this.panel1.Controls.Add(this.btnSDelete);
            this.panel1.Controls.Add(this.cmbxSYear);
            this.panel1.Controls.Add(this.txtSPhone);
            this.panel1.Controls.Add(this.txtSEmail);
            this.panel1.Controls.Add(this.txtSAddress);
            this.panel1.Controls.Add(this.dtpSDob);
            this.panel1.Controls.Add(this.radSF);
            this.panel1.Controls.Add(this.radSM);
            this.panel1.Controls.Add(this.txtSLname);
            this.panel1.Controls.Add(this.txtSFname);
            this.panel1.Controls.Add(this.txtSReg);
            this.panel1.Location = new System.Drawing.Point(46, 18);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 545);
            this.panel1.TabIndex = 0;
            // 
            // SModule6
            // 
            this.SModule6.Location = new System.Drawing.Point(674, 430);
            this.SModule6.Name = "SModule6";
            this.SModule6.Size = new System.Drawing.Size(100, 20);
            this.SModule6.TabIndex = 48;
            // 
            // SModule5
            // 
            this.SModule5.Location = new System.Drawing.Point(568, 430);
            this.SModule5.Name = "SModule5";
            this.SModule5.Size = new System.Drawing.Size(100, 20);
            this.SModule5.TabIndex = 47;
            // 
            // SModule4
            // 
            this.SModule4.Location = new System.Drawing.Point(462, 430);
            this.SModule4.Name = "SModule4";
            this.SModule4.Size = new System.Drawing.Size(100, 20);
            this.SModule4.TabIndex = 46;
            // 
            // SModule3
            // 
            this.SModule3.Location = new System.Drawing.Point(356, 430);
            this.SModule3.Name = "SModule3";
            this.SModule3.Size = new System.Drawing.Size(100, 20);
            this.SModule3.TabIndex = 45;
            // 
            // SModule2
            // 
            this.SModule2.Location = new System.Drawing.Point(250, 430);
            this.SModule2.Name = "SModule2";
            this.SModule2.Size = new System.Drawing.Size(100, 20);
            this.SModule2.TabIndex = 44;
            // 
            // SModule1
            // 
            this.SModule1.Location = new System.Drawing.Point(144, 430);
            this.SModule1.Name = "SModule1";
            this.SModule1.Size = new System.Drawing.Size(100, 20);
            this.SModule1.TabIndex = 43;
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(20, 146);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(60, 19);
            this.materialLabel4.TabIndex = 42;
            this.materialLabel4.Text = "Gender:";
            // 
            // pbxSSearch
            // 
            this.pbxSSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbxSSearch.BackgroundImage")));
            this.pbxSSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxSSearch.Location = new System.Drawing.Point(359, 43);
            this.pbxSSearch.Name = "pbxSSearch";
            this.pbxSSearch.Size = new System.Drawing.Size(24, 24);
            this.pbxSSearch.TabIndex = 41;
            this.pbxSSearch.TabStop = false;
            this.pbxSSearch.Click += new System.EventHandler(this.pbxSSearch_Click);
            // 
            // materialLabel10
            // 
            this.materialLabel10.AutoSize = true;
            this.materialLabel10.Depth = 0;
            this.materialLabel10.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel10.Location = new System.Drawing.Point(20, 431);
            this.materialLabel10.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel10.Name = "materialLabel10";
            this.materialLabel10.Size = new System.Drawing.Size(71, 19);
            this.materialLabel10.TabIndex = 27;
            this.materialLabel10.Text = "Modules:";
            // 
            // materialLabel9
            // 
            this.materialLabel9.AutoSize = true;
            this.materialLabel9.Depth = 0;
            this.materialLabel9.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel9.Location = new System.Drawing.Point(20, 385);
            this.materialLabel9.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel9.Name = "materialLabel9";
            this.materialLabel9.Size = new System.Drawing.Size(114, 19);
            this.materialLabel9.TabIndex = 26;
            this.materialLabel9.Text = "Academic Year:";
            // 
            // materialLabel8
            // 
            this.materialLabel8.AutoSize = true;
            this.materialLabel8.Depth = 0;
            this.materialLabel8.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel8.Location = new System.Drawing.Point(20, 338);
            this.materialLabel8.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel8.Name = "materialLabel8";
            this.materialLabel8.Size = new System.Drawing.Size(112, 19);
            this.materialLabel8.TabIndex = 25;
            this.materialLabel8.Text = "Phone Number:";
            // 
            // materialLabel7
            // 
            this.materialLabel7.AutoSize = true;
            this.materialLabel7.Depth = 0;
            this.materialLabel7.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel7.Location = new System.Drawing.Point(20, 290);
            this.materialLabel7.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel7.Name = "materialLabel7";
            this.materialLabel7.Size = new System.Drawing.Size(51, 19);
            this.materialLabel7.TabIndex = 24;
            this.materialLabel7.Text = "Email:";
            // 
            // materialLabel6
            // 
            this.materialLabel6.AutoSize = true;
            this.materialLabel6.Depth = 0;
            this.materialLabel6.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel6.Location = new System.Drawing.Point(20, 242);
            this.materialLabel6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel6.Name = "materialLabel6";
            this.materialLabel6.Size = new System.Drawing.Size(68, 19);
            this.materialLabel6.TabIndex = 23;
            this.materialLabel6.Text = "Address:";
            // 
            // materialLabel5
            // 
            this.materialLabel5.AutoSize = true;
            this.materialLabel5.Depth = 0;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel5.Location = new System.Drawing.Point(20, 196);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(97, 19);
            this.materialLabel5.TabIndex = 22;
            this.materialLabel5.Text = "Date of Birth:";
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(400, 94);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(86, 19);
            this.materialLabel3.TabIndex = 20;
            this.materialLabel3.Text = "Last Name:";
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(20, 94);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(87, 19);
            this.materialLabel2.TabIndex = 19;
            this.materialLabel2.Text = "First Name:";
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(20, 46);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(95, 19);
            this.materialLabel1.TabIndex = 18;
            this.materialLabel1.Text = "Reg Number:";
            // 
            // btnSSave
            // 
            this.btnSSave.AutoSize = true;
            this.btnSSave.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSSave.Depth = 0;
            this.btnSSave.Icon = null;
            this.btnSSave.Location = new System.Drawing.Point(473, 478);
            this.btnSSave.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnSSave.Name = "btnSSave";
            this.btnSSave.Primary = true;
            this.btnSSave.Size = new System.Drawing.Size(140, 36);
            this.btnSSave.TabIndex = 17;
            this.btnSSave.Text = "Save the Record";
            this.btnSSave.UseVisualStyleBackColor = true;
            this.btnSSave.Click += new System.EventHandler(this.btnSSave_Click);
            // 
            // btnSDelete
            // 
            this.btnSDelete.AutoSize = true;
            this.btnSDelete.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSDelete.Depth = 0;
            this.btnSDelete.Icon = null;
            this.btnSDelete.Location = new System.Drawing.Point(628, 478);
            this.btnSDelete.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnSDelete.Name = "btnSDelete";
            this.btnSDelete.Primary = true;
            this.btnSDelete.Size = new System.Drawing.Size(154, 36);
            this.btnSDelete.TabIndex = 16;
            this.btnSDelete.Text = "Delete the Record";
            this.btnSDelete.UseVisualStyleBackColor = true;
            this.btnSDelete.Click += new System.EventHandler(this.btnSDelete_Click);
            // 
            // cmbxSYear
            // 
            this.cmbxSYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxSYear.FormattingEnabled = true;
            this.cmbxSYear.Items.AddRange(new object[] {
            "1sr Year",
            "2nd Year",
            "3rd Year",
            "4th Year"});
            this.cmbxSYear.Location = new System.Drawing.Point(144, 384);
            this.cmbxSYear.Name = "cmbxSYear";
            this.cmbxSYear.Size = new System.Drawing.Size(200, 21);
            this.cmbxSYear.TabIndex = 9;
            // 
            // txtSPhone
            // 
            this.txtSPhone.Depth = 0;
            this.txtSPhone.Hint = "";
            this.txtSPhone.Location = new System.Drawing.Point(144, 336);
            this.txtSPhone.MaxLength = 32767;
            this.txtSPhone.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSPhone.Name = "txtSPhone";
            this.txtSPhone.PasswordChar = '\0';
            this.txtSPhone.SelectedText = "";
            this.txtSPhone.SelectionLength = 0;
            this.txtSPhone.SelectionStart = 0;
            this.txtSPhone.Size = new System.Drawing.Size(200, 23);
            this.txtSPhone.TabIndex = 8;
            this.txtSPhone.TabStop = false;
            this.txtSPhone.UseSystemPasswordChar = false;
            // 
            // txtSEmail
            // 
            this.txtSEmail.Depth = 0;
            this.txtSEmail.Hint = "";
            this.txtSEmail.Location = new System.Drawing.Point(144, 288);
            this.txtSEmail.MaxLength = 32767;
            this.txtSEmail.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSEmail.Name = "txtSEmail";
            this.txtSEmail.PasswordChar = '\0';
            this.txtSEmail.SelectedText = "";
            this.txtSEmail.SelectionLength = 0;
            this.txtSEmail.SelectionStart = 0;
            this.txtSEmail.Size = new System.Drawing.Size(200, 23);
            this.txtSEmail.TabIndex = 7;
            this.txtSEmail.TabStop = false;
            this.txtSEmail.UseSystemPasswordChar = false;
            // 
            // txtSAddress
            // 
            this.txtSAddress.Depth = 0;
            this.txtSAddress.Hint = "";
            this.txtSAddress.Location = new System.Drawing.Point(144, 240);
            this.txtSAddress.MaxLength = 32767;
            this.txtSAddress.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSAddress.Name = "txtSAddress";
            this.txtSAddress.PasswordChar = '\0';
            this.txtSAddress.SelectedText = "";
            this.txtSAddress.SelectionLength = 0;
            this.txtSAddress.SelectionStart = 0;
            this.txtSAddress.Size = new System.Drawing.Size(200, 23);
            this.txtSAddress.TabIndex = 6;
            this.txtSAddress.TabStop = false;
            this.txtSAddress.UseSystemPasswordChar = false;
            // 
            // dtpSDob
            // 
            this.dtpSDob.Location = new System.Drawing.Point(144, 195);
            this.dtpSDob.Name = "dtpSDob";
            this.dtpSDob.Size = new System.Drawing.Size(200, 20);
            this.dtpSDob.TabIndex = 5;
            // 
            // radSF
            // 
            this.radSF.AutoSize = true;
            this.radSF.Depth = 0;
            this.radSF.Font = new System.Drawing.Font("Roboto", 10F);
            this.radSF.Location = new System.Drawing.Point(223, 140);
            this.radSF.Margin = new System.Windows.Forms.Padding(0);
            this.radSF.MouseLocation = new System.Drawing.Point(-1, -1);
            this.radSF.MouseState = MaterialSkin.MouseState.HOVER;
            this.radSF.Name = "radSF";
            this.radSF.Ripple = true;
            this.radSF.Size = new System.Drawing.Size(74, 30);
            this.radSF.TabIndex = 4;
            this.radSF.Text = "Female";
            this.radSF.UseVisualStyleBackColor = true;
            // 
            // radSM
            // 
            this.radSM.AutoSize = true;
            this.radSM.Checked = true;
            this.radSM.Depth = 0;
            this.radSM.Font = new System.Drawing.Font("Roboto", 10F);
            this.radSM.Location = new System.Drawing.Point(144, 140);
            this.radSM.Margin = new System.Windows.Forms.Padding(0);
            this.radSM.MouseLocation = new System.Drawing.Point(-1, -1);
            this.radSM.MouseState = MaterialSkin.MouseState.HOVER;
            this.radSM.Name = "radSM";
            this.radSM.Ripple = true;
            this.radSM.Size = new System.Drawing.Size(59, 30);
            this.radSM.TabIndex = 3;
            this.radSM.TabStop = true;
            this.radSM.Text = "Male";
            this.radSM.UseVisualStyleBackColor = true;
            // 
            // txtSLname
            // 
            this.txtSLname.Depth = 0;
            this.txtSLname.Hint = "";
            this.txtSLname.Location = new System.Drawing.Point(524, 92);
            this.txtSLname.MaxLength = 32767;
            this.txtSLname.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSLname.Name = "txtSLname";
            this.txtSLname.PasswordChar = '\0';
            this.txtSLname.SelectedText = "";
            this.txtSLname.SelectionLength = 0;
            this.txtSLname.SelectionStart = 0;
            this.txtSLname.Size = new System.Drawing.Size(200, 23);
            this.txtSLname.TabIndex = 2;
            this.txtSLname.TabStop = false;
            this.txtSLname.UseSystemPasswordChar = false;
            // 
            // txtSFname
            // 
            this.txtSFname.Depth = 0;
            this.txtSFname.Hint = "";
            this.txtSFname.Location = new System.Drawing.Point(144, 92);
            this.txtSFname.MaxLength = 32767;
            this.txtSFname.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSFname.Name = "txtSFname";
            this.txtSFname.PasswordChar = '\0';
            this.txtSFname.SelectedText = "";
            this.txtSFname.SelectionLength = 0;
            this.txtSFname.SelectionStart = 0;
            this.txtSFname.Size = new System.Drawing.Size(200, 23);
            this.txtSFname.TabIndex = 1;
            this.txtSFname.TabStop = false;
            this.txtSFname.UseSystemPasswordChar = false;
            // 
            // txtSReg
            // 
            this.txtSReg.Depth = 0;
            this.txtSReg.Hint = "";
            this.txtSReg.Location = new System.Drawing.Point(144, 44);
            this.txtSReg.MaxLength = 32767;
            this.txtSReg.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSReg.Name = "txtSReg";
            this.txtSReg.PasswordChar = '\0';
            this.txtSReg.SelectedText = "";
            this.txtSReg.SelectionLength = 0;
            this.txtSReg.SelectionStart = 0;
            this.txtSReg.Size = new System.Drawing.Size(200, 23);
            this.txtSReg.TabIndex = 0;
            this.txtSReg.TabStop = false;
            this.txtSReg.UseSystemPasswordChar = false;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.panel2);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(892, 548);
            this.tabPage6.TabIndex = 3;
            this.tabPage6.Text = "Professors";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnPModules);
            this.panel2.Controls.Add(this.materialLabel21);
            this.panel2.Controls.Add(this.txtPUsername);
            this.panel2.Controls.Add(this.txtPQf);
            this.panel2.Controls.Add(this.materialLabel11);
            this.panel2.Controls.Add(this.pbxPSearch);
            this.panel2.Controls.Add(this.materialLabel12);
            this.panel2.Controls.Add(this.materialLabel13);
            this.panel2.Controls.Add(this.materialLabel14);
            this.panel2.Controls.Add(this.materialLabel15);
            this.panel2.Controls.Add(this.materialLabel16);
            this.panel2.Controls.Add(this.materialLabel17);
            this.panel2.Controls.Add(this.materialLabel18);
            this.panel2.Controls.Add(this.materialLabel19);
            this.panel2.Controls.Add(this.materialLabel20);
            this.panel2.Controls.Add(this.btnPSave);
            this.panel2.Controls.Add(this.btnPDelete);
            this.panel2.Controls.Add(this.txtPPhone);
            this.panel2.Controls.Add(this.txtPEmail);
            this.panel2.Controls.Add(this.txtPAddress);
            this.panel2.Controls.Add(this.dtpPDob);
            this.panel2.Controls.Add(this.radPF);
            this.panel2.Controls.Add(this.radPM);
            this.panel2.Controls.Add(this.txtPLname);
            this.panel2.Controls.Add(this.txtPFname);
            this.panel2.Controls.Add(this.txtPReg);
            this.panel2.Location = new System.Drawing.Point(46, 18);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(800, 545);
            this.panel2.TabIndex = 1;
            // 
            // btnPModules
            // 
            this.btnPModules.AutoSize = true;
            this.btnPModules.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnPModules.Depth = 0;
            this.btnPModules.Icon = null;
            this.btnPModules.Location = new System.Drawing.Point(144, 422);
            this.btnPModules.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnPModules.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnPModules.Name = "btnPModules";
            this.btnPModules.Primary = false;
            this.btnPModules.Size = new System.Drawing.Size(136, 36);
            this.btnPModules.TabIndex = 46;
            this.btnPModules.Text = "Select Modules";
            this.btnPModules.UseVisualStyleBackColor = true;
            this.btnPModules.Click += new System.EventHandler(this.btnPModules_Click);
            // 
            // materialLabel21
            // 
            this.materialLabel21.AutoSize = true;
            this.materialLabel21.Depth = 0;
            this.materialLabel21.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel21.Location = new System.Drawing.Point(400, 46);
            this.materialLabel21.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel21.Name = "materialLabel21";
            this.materialLabel21.Size = new System.Drawing.Size(81, 19);
            this.materialLabel21.TabIndex = 45;
            this.materialLabel21.Text = "Username:";
            // 
            // txtPUsername
            // 
            this.txtPUsername.Depth = 0;
            this.txtPUsername.Hint = "";
            this.txtPUsername.Location = new System.Drawing.Point(524, 44);
            this.txtPUsername.MaxLength = 32767;
            this.txtPUsername.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtPUsername.Name = "txtPUsername";
            this.txtPUsername.PasswordChar = '\0';
            this.txtPUsername.SelectedText = "";
            this.txtPUsername.SelectionLength = 0;
            this.txtPUsername.SelectionStart = 0;
            this.txtPUsername.Size = new System.Drawing.Size(200, 23);
            this.txtPUsername.TabIndex = 44;
            this.txtPUsername.TabStop = false;
            this.txtPUsername.UseSystemPasswordChar = false;
            // 
            // txtPQf
            // 
            this.txtPQf.Depth = 0;
            this.txtPQf.Hint = "";
            this.txtPQf.Location = new System.Drawing.Point(144, 383);
            this.txtPQf.MaxLength = 32767;
            this.txtPQf.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtPQf.Name = "txtPQf";
            this.txtPQf.PasswordChar = '\0';
            this.txtPQf.SelectedText = "";
            this.txtPQf.SelectionLength = 0;
            this.txtPQf.SelectionStart = 0;
            this.txtPQf.Size = new System.Drawing.Size(200, 23);
            this.txtPQf.TabIndex = 43;
            this.txtPQf.TabStop = false;
            this.txtPQf.UseSystemPasswordChar = false;
            // 
            // materialLabel11
            // 
            this.materialLabel11.AutoSize = true;
            this.materialLabel11.Depth = 0;
            this.materialLabel11.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel11.Location = new System.Drawing.Point(20, 146);
            this.materialLabel11.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel11.Name = "materialLabel11";
            this.materialLabel11.Size = new System.Drawing.Size(60, 19);
            this.materialLabel11.TabIndex = 42;
            this.materialLabel11.Text = "Gender:";
            // 
            // pbxPSearch
            // 
            this.pbxPSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbxPSearch.BackgroundImage")));
            this.pbxPSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxPSearch.Location = new System.Drawing.Point(359, 43);
            this.pbxPSearch.Name = "pbxPSearch";
            this.pbxPSearch.Size = new System.Drawing.Size(24, 24);
            this.pbxPSearch.TabIndex = 41;
            this.pbxPSearch.TabStop = false;
            this.pbxPSearch.Click += new System.EventHandler(this.pbxPSearch_Click);
            // 
            // materialLabel12
            // 
            this.materialLabel12.AutoSize = true;
            this.materialLabel12.Depth = 0;
            this.materialLabel12.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel12.Location = new System.Drawing.Point(20, 431);
            this.materialLabel12.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel12.Name = "materialLabel12";
            this.materialLabel12.Size = new System.Drawing.Size(71, 19);
            this.materialLabel12.TabIndex = 27;
            this.materialLabel12.Text = "Modules:";
            // 
            // materialLabel13
            // 
            this.materialLabel13.AutoSize = true;
            this.materialLabel13.Depth = 0;
            this.materialLabel13.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel13.Location = new System.Drawing.Point(20, 385);
            this.materialLabel13.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel13.Name = "materialLabel13";
            this.materialLabel13.Size = new System.Drawing.Size(106, 19);
            this.materialLabel13.TabIndex = 26;
            this.materialLabel13.Text = "Qualifications:";
            // 
            // materialLabel14
            // 
            this.materialLabel14.AutoSize = true;
            this.materialLabel14.Depth = 0;
            this.materialLabel14.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel14.Location = new System.Drawing.Point(20, 338);
            this.materialLabel14.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel14.Name = "materialLabel14";
            this.materialLabel14.Size = new System.Drawing.Size(112, 19);
            this.materialLabel14.TabIndex = 25;
            this.materialLabel14.Text = "Phone Number:";
            // 
            // materialLabel15
            // 
            this.materialLabel15.AutoSize = true;
            this.materialLabel15.Depth = 0;
            this.materialLabel15.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel15.Location = new System.Drawing.Point(20, 290);
            this.materialLabel15.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel15.Name = "materialLabel15";
            this.materialLabel15.Size = new System.Drawing.Size(51, 19);
            this.materialLabel15.TabIndex = 24;
            this.materialLabel15.Text = "Email:";
            // 
            // materialLabel16
            // 
            this.materialLabel16.AutoSize = true;
            this.materialLabel16.Depth = 0;
            this.materialLabel16.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel16.Location = new System.Drawing.Point(20, 242);
            this.materialLabel16.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel16.Name = "materialLabel16";
            this.materialLabel16.Size = new System.Drawing.Size(68, 19);
            this.materialLabel16.TabIndex = 23;
            this.materialLabel16.Text = "Address:";
            // 
            // materialLabel17
            // 
            this.materialLabel17.AutoSize = true;
            this.materialLabel17.Depth = 0;
            this.materialLabel17.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel17.Location = new System.Drawing.Point(20, 196);
            this.materialLabel17.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel17.Name = "materialLabel17";
            this.materialLabel17.Size = new System.Drawing.Size(97, 19);
            this.materialLabel17.TabIndex = 22;
            this.materialLabel17.Text = "Date of Birth:";
            // 
            // materialLabel18
            // 
            this.materialLabel18.AutoSize = true;
            this.materialLabel18.Depth = 0;
            this.materialLabel18.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel18.Location = new System.Drawing.Point(400, 94);
            this.materialLabel18.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel18.Name = "materialLabel18";
            this.materialLabel18.Size = new System.Drawing.Size(86, 19);
            this.materialLabel18.TabIndex = 20;
            this.materialLabel18.Text = "Last Name:";
            // 
            // materialLabel19
            // 
            this.materialLabel19.AutoSize = true;
            this.materialLabel19.Depth = 0;
            this.materialLabel19.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel19.Location = new System.Drawing.Point(20, 94);
            this.materialLabel19.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel19.Name = "materialLabel19";
            this.materialLabel19.Size = new System.Drawing.Size(87, 19);
            this.materialLabel19.TabIndex = 19;
            this.materialLabel19.Text = "First Name:";
            // 
            // materialLabel20
            // 
            this.materialLabel20.AutoSize = true;
            this.materialLabel20.Depth = 0;
            this.materialLabel20.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel20.Location = new System.Drawing.Point(20, 46);
            this.materialLabel20.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel20.Name = "materialLabel20";
            this.materialLabel20.Size = new System.Drawing.Size(95, 19);
            this.materialLabel20.TabIndex = 18;
            this.materialLabel20.Text = "Reg Number:";
            // 
            // btnPSave
            // 
            this.btnPSave.AutoSize = true;
            this.btnPSave.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnPSave.Depth = 0;
            this.btnPSave.Icon = null;
            this.btnPSave.Location = new System.Drawing.Point(473, 478);
            this.btnPSave.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnPSave.Name = "btnPSave";
            this.btnPSave.Primary = true;
            this.btnPSave.Size = new System.Drawing.Size(140, 36);
            this.btnPSave.TabIndex = 17;
            this.btnPSave.Text = "Save the Record";
            this.btnPSave.UseVisualStyleBackColor = true;
            this.btnPSave.Click += new System.EventHandler(this.btnPSave_Click);
            // 
            // btnPDelete
            // 
            this.btnPDelete.AutoSize = true;
            this.btnPDelete.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnPDelete.Depth = 0;
            this.btnPDelete.Icon = null;
            this.btnPDelete.Location = new System.Drawing.Point(628, 478);
            this.btnPDelete.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnPDelete.Name = "btnPDelete";
            this.btnPDelete.Primary = true;
            this.btnPDelete.Size = new System.Drawing.Size(154, 36);
            this.btnPDelete.TabIndex = 16;
            this.btnPDelete.Text = "Delete the Record";
            this.btnPDelete.UseVisualStyleBackColor = true;
            this.btnPDelete.Click += new System.EventHandler(this.btnPDelete_Click);
            // 
            // txtPPhone
            // 
            this.txtPPhone.Depth = 0;
            this.txtPPhone.Hint = "";
            this.txtPPhone.Location = new System.Drawing.Point(144, 336);
            this.txtPPhone.MaxLength = 32767;
            this.txtPPhone.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtPPhone.Name = "txtPPhone";
            this.txtPPhone.PasswordChar = '\0';
            this.txtPPhone.SelectedText = "";
            this.txtPPhone.SelectionLength = 0;
            this.txtPPhone.SelectionStart = 0;
            this.txtPPhone.Size = new System.Drawing.Size(200, 23);
            this.txtPPhone.TabIndex = 8;
            this.txtPPhone.TabStop = false;
            this.txtPPhone.UseSystemPasswordChar = false;
            // 
            // txtPEmail
            // 
            this.txtPEmail.Depth = 0;
            this.txtPEmail.Hint = "";
            this.txtPEmail.Location = new System.Drawing.Point(144, 288);
            this.txtPEmail.MaxLength = 32767;
            this.txtPEmail.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtPEmail.Name = "txtPEmail";
            this.txtPEmail.PasswordChar = '\0';
            this.txtPEmail.SelectedText = "";
            this.txtPEmail.SelectionLength = 0;
            this.txtPEmail.SelectionStart = 0;
            this.txtPEmail.Size = new System.Drawing.Size(200, 23);
            this.txtPEmail.TabIndex = 7;
            this.txtPEmail.TabStop = false;
            this.txtPEmail.UseSystemPasswordChar = false;
            // 
            // txtPAddress
            // 
            this.txtPAddress.Depth = 0;
            this.txtPAddress.Hint = "";
            this.txtPAddress.Location = new System.Drawing.Point(144, 240);
            this.txtPAddress.MaxLength = 32767;
            this.txtPAddress.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtPAddress.Name = "txtPAddress";
            this.txtPAddress.PasswordChar = '\0';
            this.txtPAddress.SelectedText = "";
            this.txtPAddress.SelectionLength = 0;
            this.txtPAddress.SelectionStart = 0;
            this.txtPAddress.Size = new System.Drawing.Size(200, 23);
            this.txtPAddress.TabIndex = 6;
            this.txtPAddress.TabStop = false;
            this.txtPAddress.UseSystemPasswordChar = false;
            // 
            // dtpPDob
            // 
            this.dtpPDob.Location = new System.Drawing.Point(144, 195);
            this.dtpPDob.Name = "dtpPDob";
            this.dtpPDob.Size = new System.Drawing.Size(200, 20);
            this.dtpPDob.TabIndex = 5;
            // 
            // radPF
            // 
            this.radPF.AutoSize = true;
            this.radPF.Depth = 0;
            this.radPF.Font = new System.Drawing.Font("Roboto", 10F);
            this.radPF.Location = new System.Drawing.Point(223, 140);
            this.radPF.Margin = new System.Windows.Forms.Padding(0);
            this.radPF.MouseLocation = new System.Drawing.Point(-1, -1);
            this.radPF.MouseState = MaterialSkin.MouseState.HOVER;
            this.radPF.Name = "radPF";
            this.radPF.Ripple = true;
            this.radPF.Size = new System.Drawing.Size(74, 30);
            this.radPF.TabIndex = 4;
            this.radPF.Text = "Female";
            this.radPF.UseVisualStyleBackColor = true;
            // 
            // radPM
            // 
            this.radPM.AutoSize = true;
            this.radPM.Checked = true;
            this.radPM.Depth = 0;
            this.radPM.Font = new System.Drawing.Font("Roboto", 10F);
            this.radPM.Location = new System.Drawing.Point(144, 140);
            this.radPM.Margin = new System.Windows.Forms.Padding(0);
            this.radPM.MouseLocation = new System.Drawing.Point(-1, -1);
            this.radPM.MouseState = MaterialSkin.MouseState.HOVER;
            this.radPM.Name = "radPM";
            this.radPM.Ripple = true;
            this.radPM.Size = new System.Drawing.Size(59, 30);
            this.radPM.TabIndex = 3;
            this.radPM.TabStop = true;
            this.radPM.Text = "Male";
            this.radPM.UseVisualStyleBackColor = true;
            // 
            // txtPLname
            // 
            this.txtPLname.Depth = 0;
            this.txtPLname.Hint = "";
            this.txtPLname.Location = new System.Drawing.Point(524, 92);
            this.txtPLname.MaxLength = 32767;
            this.txtPLname.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtPLname.Name = "txtPLname";
            this.txtPLname.PasswordChar = '\0';
            this.txtPLname.SelectedText = "";
            this.txtPLname.SelectionLength = 0;
            this.txtPLname.SelectionStart = 0;
            this.txtPLname.Size = new System.Drawing.Size(200, 23);
            this.txtPLname.TabIndex = 2;
            this.txtPLname.TabStop = false;
            this.txtPLname.UseSystemPasswordChar = false;
            // 
            // txtPFname
            // 
            this.txtPFname.Depth = 0;
            this.txtPFname.Hint = "";
            this.txtPFname.Location = new System.Drawing.Point(144, 92);
            this.txtPFname.MaxLength = 32767;
            this.txtPFname.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtPFname.Name = "txtPFname";
            this.txtPFname.PasswordChar = '\0';
            this.txtPFname.SelectedText = "";
            this.txtPFname.SelectionLength = 0;
            this.txtPFname.SelectionStart = 0;
            this.txtPFname.Size = new System.Drawing.Size(200, 23);
            this.txtPFname.TabIndex = 1;
            this.txtPFname.TabStop = false;
            this.txtPFname.UseSystemPasswordChar = false;
            // 
            // txtPReg
            // 
            this.txtPReg.Depth = 0;
            this.txtPReg.Hint = "";
            this.txtPReg.Location = new System.Drawing.Point(144, 44);
            this.txtPReg.MaxLength = 32767;
            this.txtPReg.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtPReg.Name = "txtPReg";
            this.txtPReg.PasswordChar = '\0';
            this.txtPReg.SelectedText = "";
            this.txtPReg.SelectionLength = 0;
            this.txtPReg.SelectionStart = 0;
            this.txtPReg.Size = new System.Drawing.Size(200, 23);
            this.txtPReg.TabIndex = 0;
            this.txtPReg.TabStop = false;
            this.txtPReg.UseSystemPasswordChar = false;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.panel7);
            this.tabPage7.Controls.Add(this.panel6);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(892, 554);
            this.tabPage7.TabIndex = 4;
            this.tabPage7.Text = "Tools";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.btnReset);
            this.panel7.Controls.Add(this.txtReset);
            this.panel7.Controls.Add(this.materialLabel36);
            this.panel7.Location = new System.Drawing.Point(46, 495);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(500, 68);
            this.panel7.TabIndex = 21;
            // 
            // btnReset
            // 
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnReset.Location = new System.Drawing.Point(322, 18);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(140, 30);
            this.btnReset.TabIndex = 22;
            this.btnReset.Text = "RESET PASSWORD";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // txtReset
            // 
            this.txtReset.Location = new System.Drawing.Point(129, 23);
            this.txtReset.Name = "txtReset";
            this.txtReset.Size = new System.Drawing.Size(180, 20);
            this.txtReset.TabIndex = 21;
            // 
            // materialLabel36
            // 
            this.materialLabel36.AutoSize = true;
            this.materialLabel36.Depth = 0;
            this.materialLabel36.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel36.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel36.Location = new System.Drawing.Point(37, 24);
            this.materialLabel36.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel36.Name = "materialLabel36";
            this.materialLabel36.Size = new System.Drawing.Size(85, 19);
            this.materialLabel36.TabIndex = 20;
            this.materialLabel36.Text = "Username: ";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.btnSearch);
            this.panel6.Controls.Add(this.datagrid);
            this.panel6.Controls.Add(this.txtValue);
            this.panel6.Controls.Add(this.cmbxSearchBy);
            this.panel6.Controls.Add(this.cmbxSearchTable);
            this.panel6.Controls.Add(this.materialLabel34);
            this.panel6.Controls.Add(this.materialLabel29);
            this.panel6.Location = new System.Drawing.Point(46, 18);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(800, 460);
            this.panel6.TabIndex = 20;
            // 
            // btnSearch
            // 
            this.btnSearch.AutoSize = true;
            this.btnSearch.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSearch.Depth = 0;
            this.btnSearch.Icon = null;
            this.btnSearch.Location = new System.Drawing.Point(627, 403);
            this.btnSearch.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Primary = true;
            this.btnSearch.Size = new System.Drawing.Size(73, 36);
            this.btnSearch.TabIndex = 5;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // datagrid
            // 
            this.datagrid.AllowUserToAddRows = false;
            this.datagrid.AllowUserToDeleteRows = false;
            this.datagrid.AllowUserToOrderColumns = true;
            this.datagrid.AllowUserToResizeRows = false;
            this.datagrid.BackgroundColor = System.Drawing.Color.White;
            this.datagrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.datagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datagrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.datagrid.EnableHeadersVisualStyles = false;
            this.datagrid.Location = new System.Drawing.Point(19, 20);
            this.datagrid.Name = "datagrid";
            this.datagrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.datagrid.ShowCellErrors = false;
            this.datagrid.ShowCellToolTips = false;
            this.datagrid.ShowEditingIcon = false;
            this.datagrid.ShowRowErrors = false;
            this.datagrid.Size = new System.Drawing.Size(760, 360);
            this.datagrid.TabIndex = 19;
            // 
            // txtValue
            // 
            this.txtValue.Location = new System.Drawing.Point(460, 411);
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(150, 20);
            this.txtValue.TabIndex = 4;
            this.txtValue.Text = "*";
            // 
            // cmbxSearchBy
            // 
            this.cmbxSearchBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxSearchBy.FormattingEnabled = true;
            this.cmbxSearchBy.Location = new System.Drawing.Point(297, 411);
            this.cmbxSearchBy.Name = "cmbxSearchBy";
            this.cmbxSearchBy.Size = new System.Drawing.Size(135, 21);
            this.cmbxSearchBy.TabIndex = 3;
            // 
            // cmbxSearchTable
            // 
            this.cmbxSearchTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxSearchTable.FormattingEnabled = true;
            this.cmbxSearchTable.Items.AddRange(new object[] {
            "Modules",
            "Students",
            "Professors"});
            this.cmbxSearchTable.Location = new System.Drawing.Point(91, 411);
            this.cmbxSearchTable.Name = "cmbxSearchTable";
            this.cmbxSearchTable.Size = new System.Drawing.Size(135, 21);
            this.cmbxSearchTable.TabIndex = 2;
            this.cmbxSearchTable.SelectedIndexChanged += new System.EventHandler(this.cmbxSearchTable_SelectedIndexChanged);
            // 
            // materialLabel34
            // 
            this.materialLabel34.AutoSize = true;
            this.materialLabel34.Depth = 0;
            this.materialLabel34.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel34.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel34.Location = new System.Drawing.Point(254, 412);
            this.materialLabel34.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel34.Name = "materialLabel34";
            this.materialLabel34.Size = new System.Drawing.Size(25, 19);
            this.materialLabel34.TabIndex = 1;
            this.materialLabel34.Text = "By";
            // 
            // materialLabel29
            // 
            this.materialLabel29.AutoSize = true;
            this.materialLabel29.Depth = 0;
            this.materialLabel29.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel29.Location = new System.Drawing.Point(18, 412);
            this.materialLabel29.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel29.Name = "materialLabel29";
            this.materialLabel29.Size = new System.Drawing.Size(55, 19);
            this.materialLabel29.TabIndex = 0;
            this.materialLabel29.Text = "Search";
            // 
            // materialTabSelector1
            // 
            this.materialTabSelector1.BaseTabControl = this.materialTabControl1;
            this.materialTabSelector1.Depth = 0;
            this.materialTabSelector1.Location = new System.Drawing.Point(0, 64);
            this.materialTabSelector1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabSelector1.Name = "materialTabSelector1";
            this.materialTabSelector1.Size = new System.Drawing.Size(900, 36);
            this.materialTabSelector1.TabIndex = 1;
            this.materialTabSelector1.Text = "materialTabSelector1";
            this.materialTabSelector1.Click += new System.EventHandler(this.MaterialTabSelector1_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 680);
            this.Controls.Add(this.materialTabSelector1);
            this.Controls.Add(this.materialTabControl1);
            this.MaximizeBox = false;
            this.Name = "Main";
            this.Sizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administrator";
            this.Load += new System.EventHandler(this.Main_Load);
            this.materialTabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMSearch)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSSearch)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxPSearch)).EndInit();
            this.tabPage7.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datagrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MaterialSkin.Controls.MaterialTabControl materialTabControl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cmbxSYear;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSPhone;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSEmail;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSAddress;
        private System.Windows.Forms.DateTimePicker dtpSDob;
        private MaterialSkin.Controls.MaterialRadioButton radSF;
        private MaterialSkin.Controls.MaterialRadioButton radSM;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSLname;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSFname;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSReg;
        private MaterialSkin.Controls.MaterialRaisedButton btnSDelete;
        private MaterialSkin.Controls.MaterialRaisedButton btnSSave;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialLabel materialLabel10;
        private MaterialSkin.Controls.MaterialLabel materialLabel9;
        private MaterialSkin.Controls.MaterialLabel materialLabel8;
        private MaterialSkin.Controls.MaterialLabel materialLabel7;
        private MaterialSkin.Controls.MaterialLabel materialLabel6;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private System.Windows.Forms.PictureBox pbxSSearch;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private System.Windows.Forms.Panel panel2;
        private MaterialSkin.Controls.MaterialLabel materialLabel11;
        private System.Windows.Forms.PictureBox pbxPSearch;
        private MaterialSkin.Controls.MaterialLabel materialLabel13;
        private MaterialSkin.Controls.MaterialLabel materialLabel14;
        private MaterialSkin.Controls.MaterialLabel materialLabel15;
        private MaterialSkin.Controls.MaterialLabel materialLabel16;
        private MaterialSkin.Controls.MaterialLabel materialLabel17;
        private MaterialSkin.Controls.MaterialLabel materialLabel18;
        private MaterialSkin.Controls.MaterialLabel materialLabel19;
        private MaterialSkin.Controls.MaterialLabel materialLabel20;
        private MaterialSkin.Controls.MaterialRaisedButton btnPSave;
        private MaterialSkin.Controls.MaterialRaisedButton btnPDelete;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtPPhone;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtPEmail;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtPAddress;
        private System.Windows.Forms.DateTimePicker dtpPDob;
        private MaterialSkin.Controls.MaterialRadioButton radPF;
        private MaterialSkin.Controls.MaterialRadioButton radPM;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtPLname;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtPFname;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtPReg;
        private MaterialSkin.Controls.MaterialLabel materialLabel21;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtPUsername;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtPQf;
        private MaterialSkin.Controls.MaterialLabel materialLabel12;
        private MaterialSkin.Controls.MaterialFlatButton btnPModules;
        private System.Windows.Forms.Panel panel3;
        private MaterialSkin.Controls.MaterialLabel materialLabel22;
        private System.Windows.Forms.PictureBox pbxMSearch;
        private MaterialSkin.Controls.MaterialLabel materialLabel26;
        private MaterialSkin.Controls.MaterialLabel materialLabel27;
        private MaterialSkin.Controls.MaterialLabel materialLabel28;
        private MaterialSkin.Controls.MaterialLabel materialLabel30;
        private MaterialSkin.Controls.MaterialLabel materialLabel31;
        private MaterialSkin.Controls.MaterialRaisedButton btnMSave;
        private MaterialSkin.Controls.MaterialRaisedButton btnMDelete;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtMCost;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtMHours;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtMName;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtMId;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtMYears;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtMCredits;
        private System.Windows.Forms.Panel panel4;
        private MaterialSkin.Controls.MaterialLabel materialLabel32;
        private MaterialSkin.Controls.MaterialLabel materialLabel33;
        private MaterialSkin.Controls.MaterialRaisedButton btnChange;
        private System.Windows.Forms.DateTimePicker dtpEnds;
        private System.Windows.Forms.DateTimePicker dtpBegins;
        private System.Windows.Forms.Panel panel5;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtNew;
        private MaterialSkin.Controls.MaterialRaisedButton btnPassword;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.DataGridView datagrid;
        private MaterialSkin.Controls.MaterialRaisedButton btnSearch;
        private System.Windows.Forms.TextBox txtValue;
        private System.Windows.Forms.ComboBox cmbxSearchBy;
        private System.Windows.Forms.ComboBox cmbxSearchTable;
        private MaterialSkin.Controls.MaterialLabel materialLabel34;
        private MaterialSkin.Controls.MaterialLabel materialLabel29;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtCurrent;
        private MaterialSkin.Controls.MaterialLabel materialLabel23;
        private MaterialSkin.Controls.MaterialLabel materialLabel25;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtUsername;
        private MaterialSkin.Controls.MaterialLabel materialLabel24;
        private MaterialSkin.Controls.MaterialRaisedButton btnUsername;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtConfirm;
        private MaterialSkin.Controls.MaterialLabel materialLabel35;
        private System.Windows.Forms.Panel panel7;
        private MaterialSkin.Controls.MaterialLabel materialLabel36;
        private System.Windows.Forms.TextBox txtReset;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.TextBox SModule6;
        private System.Windows.Forms.TextBox SModule5;
        private System.Windows.Forms.TextBox SModule4;
        private System.Windows.Forms.TextBox SModule3;
        private System.Windows.Forms.TextBox SModule2;
        private System.Windows.Forms.TextBox SModule1;
        private MaterialSkin.Controls.MaterialTabSelector materialTabSelector1;
    }
}