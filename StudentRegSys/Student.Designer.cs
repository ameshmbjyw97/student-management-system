﻿namespace StudentRegSys
{
    partial class Student
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.materialTabControl1 = new MaterialSkin.Controls.MaterialTabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnPay = new MaterialSkin.Controls.MaterialRaisedButton();
            this.lblBill = new MaterialSkin.Controls.MaterialLabel();
            this.lblBillDesc = new MaterialSkin.Controls.MaterialLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnReg = new MaterialSkin.Controls.MaterialRaisedButton();
            this.Module1 = new System.Windows.Forms.ComboBox();
            this.Module2 = new System.Windows.Forms.ComboBox();
            this.Module3 = new System.Windows.Forms.ComboBox();
            this.Module4 = new System.Windows.Forms.ComboBox();
            this.Module5 = new System.Windows.Forms.ComboBox();
            this.Module6 = new System.Windows.Forms.ComboBox();
            this.materialLabel15 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel14 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel13 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel12 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel11 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel9 = new MaterialSkin.Controls.MaterialLabel();
            this.reg = new MaterialSkin.Controls.MaterialLabel();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txtConfirm = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel35 = new MaterialSkin.Controls.MaterialLabel();
            this.txtCurrent = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtNew = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel23 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel25 = new MaterialSkin.Controls.MaterialLabel();
            this.btnPassword = new MaterialSkin.Controls.MaterialRaisedButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblYear = new MaterialSkin.Controls.MaterialLabel();
            this.lblPhone = new MaterialSkin.Controls.MaterialLabel();
            this.lblEmail = new MaterialSkin.Controls.MaterialLabel();
            this.lblAddress = new MaterialSkin.Controls.MaterialLabel();
            this.lblDOB = new MaterialSkin.Controls.MaterialLabel();
            this.lblGender = new MaterialSkin.Controls.MaterialLabel();
            this.lblReg = new MaterialSkin.Controls.MaterialLabel();
            this.lblName = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel8 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel7 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel6 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.datagrid = new System.Windows.Forms.DataGridView();
            this.lblregValue = new MaterialSkin.Controls.MaterialTabSelector();
            this.materialTabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datagrid)).BeginInit();
            this.SuspendLayout();
            // 
            // materialTabControl1
            // 
            this.materialTabControl1.Controls.Add(this.tabPage1);
            this.materialTabControl1.Controls.Add(this.tabPage3);
            this.materialTabControl1.Controls.Add(this.tabPage2);
            this.materialTabControl1.Depth = 0;
            this.materialTabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.materialTabControl1.Location = new System.Drawing.Point(0, 100);
            this.materialTabControl1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabControl1.Name = "materialTabControl1";
            this.materialTabControl1.SelectedIndex = 0;
            this.materialTabControl1.Size = new System.Drawing.Size(900, 580);
            this.materialTabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel4);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(892, 554);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Register";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.btnPay);
            this.panel4.Controls.Add(this.lblBill);
            this.panel4.Controls.Add(this.lblBillDesc);
            this.panel4.Location = new System.Drawing.Point(46, 411);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(800, 155);
            this.panel4.TabIndex = 1;
            // 
            // btnPay
            // 
            this.btnPay.AutoSize = true;
            this.btnPay.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnPay.Depth = 0;
            this.btnPay.Icon = null;
            this.btnPay.Location = new System.Drawing.Point(628, 98);
            this.btnPay.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnPay.Name = "btnPay";
            this.btnPay.Primary = true;
            this.btnPay.Size = new System.Drawing.Size(47, 36);
            this.btnPay.TabIndex = 26;
            this.btnPay.Text = "Pay";
            this.btnPay.UseVisualStyleBackColor = true;
            this.btnPay.Visible = false;
            // 
            // lblBill
            // 
            this.lblBill.AutoSize = true;
            this.lblBill.Depth = 0;
            this.lblBill.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblBill.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblBill.Location = new System.Drawing.Point(58, 67);
            this.lblBill.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblBill.Name = "lblBill";
            this.lblBill.Size = new System.Drawing.Size(77, 19);
            this.lblBill.TabIndex = 1;
            this.lblBill.Text = "Total Bill :";
            this.lblBill.Visible = false;
            // 
            // lblBillDesc
            // 
            this.lblBillDesc.AutoSize = true;
            this.lblBillDesc.Depth = 0;
            this.lblBillDesc.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblBillDesc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblBillDesc.Location = new System.Drawing.Point(228, 67);
            this.lblBillDesc.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblBillDesc.Name = "lblBillDesc";
            this.lblBillDesc.Size = new System.Drawing.Size(342, 19);
            this.lblBillDesc.TabIndex = 0;
            this.lblBillDesc.Text = "Bill will be issued after the registration is finalized";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnReg);
            this.panel2.Controls.Add(this.Module1);
            this.panel2.Controls.Add(this.Module2);
            this.panel2.Controls.Add(this.Module3);
            this.panel2.Controls.Add(this.Module4);
            this.panel2.Controls.Add(this.Module5);
            this.panel2.Controls.Add(this.Module6);
            this.panel2.Controls.Add(this.materialLabel15);
            this.panel2.Controls.Add(this.materialLabel14);
            this.panel2.Controls.Add(this.materialLabel13);
            this.panel2.Controls.Add(this.materialLabel12);
            this.panel2.Controls.Add(this.materialLabel11);
            this.panel2.Controls.Add(this.materialLabel9);
            this.panel2.Controls.Add(this.reg);
            this.panel2.Location = new System.Drawing.Point(46, 18);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(800, 387);
            this.panel2.TabIndex = 0;
            // 
            // btnReg
            // 
            this.btnReg.AutoSize = true;
            this.btnReg.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnReg.Depth = 0;
            this.btnReg.Icon = null;
            this.btnReg.Location = new System.Drawing.Point(628, 334);
            this.btnReg.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnReg.Name = "btnReg";
            this.btnReg.Primary = true;
            this.btnReg.Size = new System.Drawing.Size(83, 36);
            this.btnReg.TabIndex = 25;
            this.btnReg.Text = "Register";
            this.btnReg.UseVisualStyleBackColor = true;
            this.btnReg.Click += new System.EventHandler(this.btnReg_Click);
            // 
            // Module1
            // 
            this.Module1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Module1.FormattingEnabled = true;
            this.Module1.Location = new System.Drawing.Point(238, 65);
            this.Module1.Name = "Module1";
            this.Module1.Size = new System.Drawing.Size(200, 21);
            this.Module1.TabIndex = 24;
            // 
            // Module2
            // 
            this.Module2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Module2.FormattingEnabled = true;
            this.Module2.Location = new System.Drawing.Point(238, 112);
            this.Module2.Name = "Module2";
            this.Module2.Size = new System.Drawing.Size(200, 21);
            this.Module2.TabIndex = 23;
            // 
            // Module3
            // 
            this.Module3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Module3.FormattingEnabled = true;
            this.Module3.Location = new System.Drawing.Point(238, 159);
            this.Module3.Name = "Module3";
            this.Module3.Size = new System.Drawing.Size(200, 21);
            this.Module3.TabIndex = 22;
            // 
            // Module4
            // 
            this.Module4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Module4.FormattingEnabled = true;
            this.Module4.Location = new System.Drawing.Point(238, 206);
            this.Module4.Name = "Module4";
            this.Module4.Size = new System.Drawing.Size(200, 21);
            this.Module4.TabIndex = 21;
            // 
            // Module5
            // 
            this.Module5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Module5.FormattingEnabled = true;
            this.Module5.Location = new System.Drawing.Point(238, 253);
            this.Module5.Name = "Module5";
            this.Module5.Size = new System.Drawing.Size(200, 21);
            this.Module5.TabIndex = 20;
            // 
            // Module6
            // 
            this.Module6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Module6.FormattingEnabled = true;
            this.Module6.Location = new System.Drawing.Point(238, 300);
            this.Module6.Name = "Module6";
            this.Module6.Size = new System.Drawing.Size(200, 21);
            this.Module6.TabIndex = 19;
            // 
            // materialLabel15
            // 
            this.materialLabel15.AutoSize = true;
            this.materialLabel15.Depth = 0;
            this.materialLabel15.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel15.Location = new System.Drawing.Point(58, 301);
            this.materialLabel15.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel15.Name = "materialLabel15";
            this.materialLabel15.Size = new System.Drawing.Size(148, 19);
            this.materialLabel15.TabIndex = 18;
            this.materialLabel15.Text = "Alternative Choice 2:";
            // 
            // materialLabel14
            // 
            this.materialLabel14.AutoSize = true;
            this.materialLabel14.Depth = 0;
            this.materialLabel14.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel14.Location = new System.Drawing.Point(58, 254);
            this.materialLabel14.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel14.Name = "materialLabel14";
            this.materialLabel14.Size = new System.Drawing.Size(148, 19);
            this.materialLabel14.TabIndex = 17;
            this.materialLabel14.Text = "Alternative Choice 1:";
            // 
            // materialLabel13
            // 
            this.materialLabel13.AutoSize = true;
            this.materialLabel13.Depth = 0;
            this.materialLabel13.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel13.Location = new System.Drawing.Point(58, 207);
            this.materialLabel13.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel13.Name = "materialLabel13";
            this.materialLabel13.Size = new System.Drawing.Size(75, 19);
            this.materialLabel13.TabIndex = 16;
            this.materialLabel13.Text = "Module 4:";
            // 
            // materialLabel12
            // 
            this.materialLabel12.AutoSize = true;
            this.materialLabel12.Depth = 0;
            this.materialLabel12.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel12.Location = new System.Drawing.Point(58, 160);
            this.materialLabel12.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel12.Name = "materialLabel12";
            this.materialLabel12.Size = new System.Drawing.Size(75, 19);
            this.materialLabel12.TabIndex = 15;
            this.materialLabel12.Text = "Module 3:";
            // 
            // materialLabel11
            // 
            this.materialLabel11.AutoSize = true;
            this.materialLabel11.Depth = 0;
            this.materialLabel11.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel11.Location = new System.Drawing.Point(58, 113);
            this.materialLabel11.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel11.Name = "materialLabel11";
            this.materialLabel11.Size = new System.Drawing.Size(75, 19);
            this.materialLabel11.TabIndex = 14;
            this.materialLabel11.Text = "Module 2:";
            // 
            // materialLabel9
            // 
            this.materialLabel9.AutoSize = true;
            this.materialLabel9.Depth = 0;
            this.materialLabel9.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel9.Location = new System.Drawing.Point(58, 66);
            this.materialLabel9.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel9.Name = "materialLabel9";
            this.materialLabel9.Size = new System.Drawing.Size(75, 19);
            this.materialLabel9.TabIndex = 13;
            this.materialLabel9.Text = "Module 1:";
            // 
            // reg
            // 
            this.reg.AutoSize = true;
            this.reg.Depth = 0;
            this.reg.Font = new System.Drawing.Font("Roboto", 11F);
            this.reg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.reg.Location = new System.Drawing.Point(489, 13);
            this.reg.MouseState = MaterialSkin.MouseState.HOVER;
            this.reg.Name = "reg";
            this.reg.Size = new System.Drawing.Size(151, 19);
            this.reg.TabIndex = 0;
            this.reg.Text = "Registration ends on:";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel5);
            this.tabPage3.Controls.Add(this.panel1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(892, 554);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "Settings";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.txtConfirm);
            this.panel5.Controls.Add(this.materialLabel35);
            this.panel5.Controls.Add(this.txtCurrent);
            this.panel5.Controls.Add(this.txtNew);
            this.panel5.Controls.Add(this.materialLabel23);
            this.panel5.Controls.Add(this.materialLabel25);
            this.panel5.Controls.Add(this.btnPassword);
            this.panel5.Location = new System.Drawing.Point(46, 398);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(800, 168);
            this.panel5.TabIndex = 4;
            // 
            // txtConfirm
            // 
            this.txtConfirm.Depth = 0;
            this.txtConfirm.Hint = "";
            this.txtConfirm.Location = new System.Drawing.Point(206, 101);
            this.txtConfirm.MaxLength = 32767;
            this.txtConfirm.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtConfirm.Name = "txtConfirm";
            this.txtConfirm.PasswordChar = '\0';
            this.txtConfirm.SelectedText = "";
            this.txtConfirm.SelectionLength = 0;
            this.txtConfirm.SelectionStart = 0;
            this.txtConfirm.Size = new System.Drawing.Size(200, 23);
            this.txtConfirm.TabIndex = 56;
            this.txtConfirm.TabStop = false;
            this.txtConfirm.UseSystemPasswordChar = true;
            // 
            // materialLabel35
            // 
            this.materialLabel35.AutoSize = true;
            this.materialLabel35.Depth = 0;
            this.materialLabel35.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel35.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel35.Location = new System.Drawing.Point(40, 103);
            this.materialLabel35.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel35.Name = "materialLabel35";
            this.materialLabel35.Size = new System.Drawing.Size(137, 19);
            this.materialLabel35.TabIndex = 55;
            this.materialLabel35.Text = "Confirm Password:";
            // 
            // txtCurrent
            // 
            this.txtCurrent.Depth = 0;
            this.txtCurrent.Hint = "";
            this.txtCurrent.Location = new System.Drawing.Point(206, 19);
            this.txtCurrent.MaxLength = 32767;
            this.txtCurrent.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtCurrent.Name = "txtCurrent";
            this.txtCurrent.PasswordChar = '\0';
            this.txtCurrent.SelectedText = "";
            this.txtCurrent.SelectionLength = 0;
            this.txtCurrent.SelectionStart = 0;
            this.txtCurrent.Size = new System.Drawing.Size(200, 23);
            this.txtCurrent.TabIndex = 51;
            this.txtCurrent.TabStop = false;
            this.txtCurrent.UseSystemPasswordChar = true;
            // 
            // txtNew
            // 
            this.txtNew.Depth = 0;
            this.txtNew.Hint = "";
            this.txtNew.Location = new System.Drawing.Point(206, 60);
            this.txtNew.MaxLength = 32767;
            this.txtNew.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtNew.Name = "txtNew";
            this.txtNew.PasswordChar = '\0';
            this.txtNew.SelectedText = "";
            this.txtNew.SelectionLength = 0;
            this.txtNew.SelectionStart = 0;
            this.txtNew.Size = new System.Drawing.Size(200, 23);
            this.txtNew.TabIndex = 50;
            this.txtNew.TabStop = false;
            this.txtNew.UseSystemPasswordChar = true;
            // 
            // materialLabel23
            // 
            this.materialLabel23.AutoSize = true;
            this.materialLabel23.Depth = 0;
            this.materialLabel23.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel23.Location = new System.Drawing.Point(40, 21);
            this.materialLabel23.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel23.Name = "materialLabel23";
            this.materialLabel23.Size = new System.Drawing.Size(132, 19);
            this.materialLabel23.TabIndex = 49;
            this.materialLabel23.Text = "Current Password:";
            // 
            // materialLabel25
            // 
            this.materialLabel25.AutoSize = true;
            this.materialLabel25.Depth = 0;
            this.materialLabel25.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel25.Location = new System.Drawing.Point(40, 62);
            this.materialLabel25.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel25.Name = "materialLabel25";
            this.materialLabel25.Size = new System.Drawing.Size(113, 19);
            this.materialLabel25.TabIndex = 47;
            this.materialLabel25.Text = "New Password:";
            // 
            // btnPassword
            // 
            this.btnPassword.AutoSize = true;
            this.btnPassword.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnPassword.Depth = 0;
            this.btnPassword.Icon = null;
            this.btnPassword.Location = new System.Drawing.Point(628, 118);
            this.btnPassword.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnPassword.Name = "btnPassword";
            this.btnPassword.Primary = true;
            this.btnPassword.Size = new System.Drawing.Size(153, 36);
            this.btnPassword.TabIndex = 46;
            this.btnPassword.Text = "Change Password";
            this.btnPassword.UseVisualStyleBackColor = true;
            this.btnPassword.Click += new System.EventHandler(this.btnPassword_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblYear);
            this.panel1.Controls.Add(this.lblPhone);
            this.panel1.Controls.Add(this.lblEmail);
            this.panel1.Controls.Add(this.lblAddress);
            this.panel1.Controls.Add(this.lblDOB);
            this.panel1.Controls.Add(this.lblGender);
            this.panel1.Controls.Add(this.lblReg);
            this.panel1.Controls.Add(this.lblName);
            this.panel1.Controls.Add(this.materialLabel8);
            this.panel1.Controls.Add(this.materialLabel7);
            this.panel1.Controls.Add(this.materialLabel6);
            this.panel1.Controls.Add(this.materialLabel5);
            this.panel1.Controls.Add(this.materialLabel4);
            this.panel1.Controls.Add(this.materialLabel3);
            this.panel1.Controls.Add(this.materialLabel2);
            this.panel1.Controls.Add(this.materialLabel1);
            this.panel1.Location = new System.Drawing.Point(46, 18);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 374);
            this.panel1.TabIndex = 1;
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.Depth = 0;
            this.lblYear.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblYear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblYear.Location = new System.Drawing.Point(202, 331);
            this.lblYear.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(110, 19);
            this.lblYear.TabIndex = 15;
            this.lblYear.Text = "Academic Year";
            this.lblYear.Click += new System.EventHandler(this.lblYear_Click);
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Depth = 0;
            this.lblPhone.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblPhone.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblPhone.Location = new System.Drawing.Point(202, 287);
            this.lblPhone.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(108, 19);
            this.lblPhone.TabIndex = 14;
            this.lblPhone.Text = "Phone Number";
            this.lblPhone.Click += new System.EventHandler(this.lblYear_Click);
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Depth = 0;
            this.lblEmail.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblEmail.Location = new System.Drawing.Point(202, 243);
            this.lblEmail.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(106, 19);
            this.lblEmail.TabIndex = 13;
            this.lblEmail.Text = "Email Address";
            this.lblEmail.Click += new System.EventHandler(this.lblYear_Click);
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Depth = 0;
            this.lblAddress.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblAddress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblAddress.Location = new System.Drawing.Point(202, 199);
            this.lblAddress.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(64, 19);
            this.lblAddress.TabIndex = 12;
            this.lblAddress.Text = "Address";
            this.lblAddress.Click += new System.EventHandler(this.lblYear_Click);
            // 
            // lblDOB
            // 
            this.lblDOB.AutoSize = true;
            this.lblDOB.Depth = 0;
            this.lblDOB.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblDOB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblDOB.Location = new System.Drawing.Point(202, 155);
            this.lblDOB.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblDOB.Name = "lblDOB";
            this.lblDOB.Size = new System.Drawing.Size(93, 19);
            this.lblDOB.TabIndex = 11;
            this.lblDOB.Text = "Date of Birth";
            this.lblDOB.Click += new System.EventHandler(this.lblYear_Click);
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Depth = 0;
            this.lblGender.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblGender.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblGender.Location = new System.Drawing.Point(202, 111);
            this.lblGender.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(56, 19);
            this.lblGender.TabIndex = 10;
            this.lblGender.Text = "Gender";
            this.lblGender.Click += new System.EventHandler(this.lblYear_Click);
            // 
            // lblReg
            // 
            this.lblReg.AutoSize = true;
            this.lblReg.Depth = 0;
            this.lblReg.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblReg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblReg.Location = new System.Drawing.Point(202, 23);
            this.lblReg.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblReg.Name = "lblReg";
            this.lblReg.Size = new System.Drawing.Size(121, 19);
            this.lblReg.TabIndex = 9;
            this.lblReg.Text = "Register Number";
            this.lblReg.Click += new System.EventHandler(this.lblYear_Click);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Depth = 0;
            this.lblName.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblName.Location = new System.Drawing.Point(202, 67);
            this.lblName.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(77, 19);
            this.lblName.TabIndex = 8;
            this.lblName.Text = "Full Name";
            this.lblName.Click += new System.EventHandler(this.lblYear_Click);
            // 
            // materialLabel8
            // 
            this.materialLabel8.AutoSize = true;
            this.materialLabel8.Depth = 0;
            this.materialLabel8.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel8.Location = new System.Drawing.Point(40, 331);
            this.materialLabel8.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel8.Name = "materialLabel8";
            this.materialLabel8.Size = new System.Drawing.Size(114, 19);
            this.materialLabel8.TabIndex = 7;
            this.materialLabel8.Text = "Academic Year:";
            // 
            // materialLabel7
            // 
            this.materialLabel7.AutoSize = true;
            this.materialLabel7.Depth = 0;
            this.materialLabel7.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel7.Location = new System.Drawing.Point(40, 287);
            this.materialLabel7.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel7.Name = "materialLabel7";
            this.materialLabel7.Size = new System.Drawing.Size(112, 19);
            this.materialLabel7.TabIndex = 6;
            this.materialLabel7.Text = "Phone Number:";
            // 
            // materialLabel6
            // 
            this.materialLabel6.AutoSize = true;
            this.materialLabel6.Depth = 0;
            this.materialLabel6.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel6.Location = new System.Drawing.Point(40, 243);
            this.materialLabel6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel6.Name = "materialLabel6";
            this.materialLabel6.Size = new System.Drawing.Size(110, 19);
            this.materialLabel6.TabIndex = 5;
            this.materialLabel6.Text = "Email Address:";
            // 
            // materialLabel5
            // 
            this.materialLabel5.AutoSize = true;
            this.materialLabel5.Depth = 0;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel5.Location = new System.Drawing.Point(40, 199);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(68, 19);
            this.materialLabel5.TabIndex = 4;
            this.materialLabel5.Text = "Address:";
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(40, 155);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(97, 19);
            this.materialLabel4.TabIndex = 3;
            this.materialLabel4.Text = "Date of Birth:";
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(40, 111);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(60, 19);
            this.materialLabel3.TabIndex = 2;
            this.materialLabel3.Text = "Gender:";
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(40, 23);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(125, 19);
            this.materialLabel2.TabIndex = 1;
            this.materialLabel2.Text = "Register Number:";
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(40, 67);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(81, 19);
            this.materialLabel1.TabIndex = 0;
            this.materialLabel1.Text = "Full Name:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.datagrid);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(892, 554);
            this.tabPage2.TabIndex = 2;
            this.tabPage2.Text = "Modules";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // datagrid
            // 
            this.datagrid.AllowUserToAddRows = false;
            this.datagrid.AllowUserToDeleteRows = false;
            this.datagrid.AllowUserToOrderColumns = true;
            this.datagrid.AllowUserToResizeColumns = false;
            this.datagrid.AllowUserToResizeRows = false;
            this.datagrid.BackgroundColor = System.Drawing.Color.White;
            this.datagrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.datagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datagrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.datagrid.EnableHeadersVisualStyles = false;
            this.datagrid.Location = new System.Drawing.Point(46, 18);
            this.datagrid.Name = "datagrid";
            this.datagrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.datagrid.ShowCellErrors = false;
            this.datagrid.ShowCellToolTips = false;
            this.datagrid.ShowEditingIcon = false;
            this.datagrid.ShowRowErrors = false;
            this.datagrid.Size = new System.Drawing.Size(800, 548);
            this.datagrid.TabIndex = 20;
            // 
            // lblregValue
            // 
            this.lblregValue.BaseTabControl = this.materialTabControl1;
            this.lblregValue.Depth = 0;
            this.lblregValue.Location = new System.Drawing.Point(0, 64);
            this.lblregValue.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblregValue.Name = "lblregValue";
            this.lblregValue.Size = new System.Drawing.Size(900, 36);
            this.lblregValue.TabIndex = 2;
            this.lblregValue.Click += new System.EventHandler(this.LblregValue_Click);
            // 
            // Student
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 680);
            this.Controls.Add(this.lblregValue);
            this.Controls.Add(this.materialTabControl1);
            this.MaximizeBox = false;
            this.Name = "Student";
            this.Text = "Student";
            this.Load += new System.EventHandler(this.Student_Load);
            this.materialTabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.datagrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MaterialSkin.Controls.MaterialTabControl materialTabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private MaterialSkin.Controls.MaterialTabSelector lblregValue;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panel1;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialLabel lblYear;
        private MaterialSkin.Controls.MaterialLabel lblPhone;
        private MaterialSkin.Controls.MaterialLabel lblEmail;
        private MaterialSkin.Controls.MaterialLabel lblAddress;
        private MaterialSkin.Controls.MaterialLabel lblDOB;
        private MaterialSkin.Controls.MaterialLabel lblGender;
        private MaterialSkin.Controls.MaterialLabel lblReg;
        private MaterialSkin.Controls.MaterialLabel lblName;
        private MaterialSkin.Controls.MaterialLabel materialLabel8;
        private MaterialSkin.Controls.MaterialLabel materialLabel7;
        private MaterialSkin.Controls.MaterialLabel materialLabel6;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private System.Windows.Forms.Panel panel5;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtConfirm;
        private MaterialSkin.Controls.MaterialLabel materialLabel35;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtCurrent;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtNew;
        private MaterialSkin.Controls.MaterialLabel materialLabel23;
        private MaterialSkin.Controls.MaterialLabel materialLabel25;
        private MaterialSkin.Controls.MaterialRaisedButton btnPassword;
        private System.Windows.Forms.Panel panel2;
        private MaterialSkin.Controls.MaterialLabel reg;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView datagrid;
        private MaterialSkin.Controls.MaterialRaisedButton btnReg;
        private System.Windows.Forms.ComboBox Module1;
        private System.Windows.Forms.ComboBox Module2;
        private System.Windows.Forms.ComboBox Module3;
        private System.Windows.Forms.ComboBox Module4;
        private System.Windows.Forms.ComboBox Module5;
        private System.Windows.Forms.ComboBox Module6;
        private MaterialSkin.Controls.MaterialLabel materialLabel15;
        private MaterialSkin.Controls.MaterialLabel materialLabel14;
        private MaterialSkin.Controls.MaterialLabel materialLabel13;
        private MaterialSkin.Controls.MaterialLabel materialLabel12;
        private MaterialSkin.Controls.MaterialLabel materialLabel11;
        private MaterialSkin.Controls.MaterialLabel materialLabel9;
        private MaterialSkin.Controls.MaterialRaisedButton btnPay;
        private MaterialSkin.Controls.MaterialLabel lblBill;
        private MaterialSkin.Controls.MaterialLabel lblBillDesc;
    }
}